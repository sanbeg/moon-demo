package com.sanbeg.android.moondemo;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static final String KEY_MODE     = "mode";
    private static final String KEY_SUBTITLE = "subtitle";

    private enum Location {EARTH, MOON};

    private enum Mode {
        NO_ANIMATION(null),
        ANIMATED(new LinearInterpolator()),
        INTERPOLATED(new AccelerateDecelerateInterpolator()),
        DECELERATE(new DecelerateInterpolator(1.2f)),
        OVERSHOOT(new OvershootInterpolator(0.8f)),
        MULTI(new DecelerateInterpolator(1.5f)),
        COMPOSE(new ComposedInterpolator());

        Mode(Interpolator interpolator) {
            this.interpolator = interpolator;
        }

        final Interpolator interpolator;
    }

    public static class ComposedInterpolator implements Interpolator {
        private final TimeInterpolator i1 = new DecelerateInterpolator(2);
        private final TimeInterpolator i2 = new OvershootInterpolator(1.2f);

        @Override
        public float getInterpolation(float input) {
            return i1.getInterpolation(input) * i2.getInterpolation(input);
        }
    }

    public static class ReverseInterpolator implements Interpolator {
        private final Interpolator interpolator;

        public ReverseInterpolator(Interpolator interpolator) {
            this.interpolator = interpolator;
        }

        @Override
        public float getInterpolation(float input) {
            return 1 - interpolator.getInterpolation(1 - input);
        }
    }

    private View moon;
    private View astronaut;
    private View sky;

    private float translationX;
    private float translationY;
    private float scale = 1;

    private Location location = Location.EARTH;
    private Mode mode;

    private int duration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            mode = Mode.valueOf(savedInstanceState.getString(KEY_MODE));
            getSupportActionBar().setSubtitle(savedInstanceState.getCharSequence(KEY_SUBTITLE));
        } else {
            mode = Mode.ANIMATED;
            getSupportActionBar().setSubtitle(getString(R.string.animated));
        }

        moon      = findViewById(R.id.moon);
        sky       = findViewById(R.id.sky);
        astronaut = findViewById(R.id.astronaut);
        duration  = getResources().getInteger(R.integer.duration);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_MODE, mode.toString());
        outState.putCharSequence(KEY_SUBTITLE, getSupportActionBar().getSubtitle());
    }

    private void updatePos() {
        switch (location) {
            case EARTH:
                float astrox = astronaut.getLeft() + astronaut.getWidth() / 2.0f;
                float astroy = astronaut.getBottom();
                float moonx  = moon.getLeft() + moon.getWidth()  * 0.6f;
                float moony  = moon.getTop()  + moon.getHeight() * 0.3f;

                translationX = moonx - astrox;
                translationY = moony - astroy;
                scale        = 0.8f;
                location     = Location.MOON;

                if (sky.getAlpha() > 0.5) {
                    translationY /= 2;
                    translationX /=2;
                    scale = 0.9f;
                }
                break;
            case MOON:
                translationX = 0;
                translationY = 0;
                scale        = 1;
                location     = Location.EARTH;
        }
    }

    public void onClickMoon(View view) {

        if (view.getId() == R.id.moon) {
            float alpha = moon.getAlpha() > 0.5 ? 0 : 1;
            moon.animate()
                    .alpha(alpha)
                    .setDuration(duration)
                    .setInterpolator(mode.interpolator);
            return;
        }

        updatePos();
        switch (mode) {
            case MULTI:
                animateObject(mode.interpolator, new AccelerateInterpolator(1.2f))
                        .setDuration(duration)
                        .start();
                break;
            case COMPOSE:
                animateObject(mode.interpolator, new ReverseInterpolator(mode.interpolator))
                        .setDuration(duration)
                        .start();
                break;
            case NO_ANIMATION:
                move();
                break;
            default:
                animate()
                        .setInterpolator(mode.interpolator)
                        .setDuration(duration)
                        .start();
        }
    }

    public void move() {
        astronaut.setTranslationX(translationX);
        astronaut.setTranslationY(translationY);
        astronaut.setScaleX(scale);
        astronaut.setScaleY(scale);
    }

    public ViewPropertyAnimator animate() {
        return astronaut.animate()
                .translationX(translationX)
                .translationY(translationY)
                .scaleX(scale)
                .scaleY(scale);
    }

    public Animator animateObject(Interpolator up, Interpolator down) {
        ObjectAnimator xanim  = ObjectAnimator.ofFloat(astronaut, "translationX", translationX);
        ObjectAnimator yanim  = ObjectAnimator.ofFloat(astronaut, "translationY", translationY);
        ObjectAnimator sxanim = ObjectAnimator.ofFloat(astronaut, "scaleX", scale);
        ObjectAnimator syanim = ObjectAnimator.ofFloat(astronaut, "scaleY", scale);

        xanim.setInterpolator(new AccelerateDecelerateInterpolator());
        yanim.setInterpolator((location == Location.MOON) ? up : down);

        Interpolator sinterp = new AccelerateDecelerateInterpolator();
        sxanim.setInterpolator(sinterp);
        syanim.setInterpolator(sinterp);

        AnimatorSet set =  new AnimatorSet();
        set.playTogether(sxanim, syanim, xanim, yanim);
        return set;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.none:
                mode = Mode.NO_ANIMATION;
                break;
            case R.id.linear:
                mode = Mode.ANIMATED;
                break;
            case R.id.accdec:
                mode = Mode.INTERPOLATED;
                break;
            case R.id.decelerate:
                mode = Mode.DECELERATE;
                break;
            case R.id.overshoot:
                mode = Mode.OVERSHOOT;
                break;
            case R.id.multiple:
                mode = Mode.MULTI;
                break;
            case R.id.compose:
                mode = Mode.COMPOSE;
                break;
            case R.id.day:
                if (sky.getVisibility() != View.VISIBLE) {
                    sky.setAlpha(0);
                    sky.setVisibility(View.VISIBLE);
                }
                float alpha = sky.getAlpha() > 0.5 ? 0 : 1;
                sky.animate()
                        .alpha(alpha)
                        .setDuration(1500)
                        .setInterpolator(mode.interpolator);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        if (item.getItemId() != R.id.day) {
            getSupportActionBar().setSubtitle(item.getTitle());
        }
        return true;
    }
}
